CSE.30341.FA17: Project 03
==========================

This is the documentation for [Project 03] of [CSE.30341.FA17].

Members
-------

1. Gaurav Sirdeshpande (gsirdesh@nd.edu)
2. Abraham Leon (jleon4@nd.edu)
3. Jared Diesslin (jdiessli@nd.edu)

Design
------

> 1. The client library needs to provide a `Client` class.
>
>   - What does the `Client` class need to keep track of?
>
>   - How will the `Client` connect to the server?
>
>   - How will it implement concurrent publishing, retrieval, and processing of
>     messages?
>
>   - How will it pass messages between different threads?
>
>   - What data members need to have access synchronized? What primitives will
>     you use to implement these?
>
>   - How will threads know when to block or to quit?
>
>   - How will the `Client` determine which `Callbacks` belong to which topics?

The client class will need to keep track of the number of threads being used. It will 
	also need to keep track of 5 operations that the client can perform. 
In order to connect to the server, we will use a simple TCP socket dial on a host and 
	port number. We can use the echo_client.c file from last semester in order to 
	connect to the server. 
It will implement concurrent publishing, retrieval and processing of messages by using 
	multiple threads. 
To pass messages between threads, we can use concurrent data structures with locks and 
	condition variables. The messages will need to have access synchronized with 
	semaphores. 
Threads should block or quit depending on the type of command requested. 
The client will determine which callbacks belong to which topic based on the type of 
	command requested.


> 2. The client library needs to provide a `Callback` class.
>
>   - What does the `Callback` class need to keep track of?
>
>   - How will applications use this `Callback` class?

The callback class will need to keep track of the commands reuested by the client. 
Depending on the application, a different handler of callback will be used.


> 3. The client library needs to provide a `Message` struct.
>
>   - What does the `Message` struct need to keep track of?
>
>   - What methods would be useful to have in processing `Messages`?

The Message struct will need to keep track of which thread it is on. It will also need 
	to track the type of command (MESSAGE, IDENTIFY, SUBSCRIBE, UNSUBSCRIBE, 
	RETRIEVE, DISCONNECT), the topic of the message, sender of the message, the sender's 
	nonce, and the body of the message.
The methods that would be useful to have in processing 'Messages' would be string type, 
	string topic, string sender, size_t nonce, and string body. 

> 4. The client library needs to provide a `Thread` class.
>
>   - What does the `Thread` class need to keep track of?
>
>   - What POSIX thread functions will need to utilize?

The 'Thread' class needs to keep track of the function and argument that are used to 
	start the thread and the resulting return value from joining the thread. 
The POSIX thread functions that will need to be utilized are: publishing: push; 
	retrieving: start, join, and detach; and callbacks: select and pull. 

> 5. You will need to perform testing on your client library.
>
>   - How will you test your client library?
>
>   - What will `echo_test` tell you?
>
>   - How will you use the Google Test framework?
>
>   - How will you incorporate testing at every stage of development?

We will test the client library by passing in various combinations of an action 
	(publish, retrieve, etc.), a topic, a length, a user ID, and a nonce. The 
	combination of variables to be passed in will depend on the action being tested. 
The echo_test will say: whether or not the connection was successful; if it was 
	successful, to which port it connected; identify as the user's username along 
	with a nonce to follow (a random number between 0 and 1000); and perform whichever 
	action (subscribe, unsubscribe, etc.) is specified in the command. 
We will use the Google Test framework by testing for each of our classes. For some of 
	the classes, this will involve creating a test struct and poulating its fields 
	and subsequently checking to see if the fields match the expected outcomes. For 
	others, it will involve calling the run function many times and incrementing a 
	counter to check if the counter ends up being the same as what it should be. 
We will incorporate testing at every stage of development by using the Google Test 
	framework, as outlined above, as well as writing a test to test all of our 
	concurrent data structures. 

> 6. You will need to create an user application that takes advantage of the
>    pub/sub system.
>
>   - What distributed and parallel application do you plan on building?
>
>   - How will utilize the client library?
>
>   - What topics will you need?
>
>   - What callbacks will you need?
>
>   - What additional threads will you need?

The distributed and parallel application we plan on building is a messaging app. 
We will utilize the client library by using its POSIX thread functions to publish, 
	retrieve, and callback the information input by the user and output a given 
	response via the messaging app. 
Our app uses topics in order for users to subscribe to it, and the users to subscribe to
	any topic they desire. This topic serves as the group name. 
The callbacks we will need are called app_callback, which is a class that derives from 
	the main callback class. What it does is displays the sender and displays the body 
	of the message. 
The additional threads we will need are from the messanger function that uses poll so 
	so that as long as the client does not shut down, it will read from stdin to use 
	the poll function to prevent blocking, which allows back and forth communication
	from two clients. Then, it publishes to whatever server the client says. 

Demonstration
-------------

> https://docs.google.com/presentation/d/1S0ERQ7AzzHtwjAHQMDwrnBld0gBO2s8XKxflZNZmpck/edit#slide=id.g28eb0ebcc1_0_419

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 03]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project03.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
