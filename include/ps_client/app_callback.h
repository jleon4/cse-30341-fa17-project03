//app_callback.h
#pragma once
#include <iostream>
using namespace std;


class App_callback : public Callback {
	void run(Message &m) {
		cout << m.sender << " says: " << m.body << endl; 
	}
};
