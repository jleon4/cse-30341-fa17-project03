//Callback.h
#pragma once
#include <iostream>
#include "ps_client/message.h"
using namespace std;

class Callback {
	public:
		virtual void run(Message &m) = 0;

	private:
};


class EchoCallback : public Callback {
	void run(Message &m) {
		if (m.type == "UNSUBSCRIBE") {
			fprintf(stdout, "UNSUBSCRIBE %s\n", m.topic.c_str());
		}
		else if (m.type == "IDENTIFY") {
			fprintf(stdout, "IDENTIFY %s %ld\n", m.sender.c_str(), m.nonce);
		}
		else if (m.type == "SUBSCRIBE") {
			fprintf(stdout, "SUBSCRIBE %s\n", m.topic.c_str());
		}
		else if (m.type == "PUBLISH") {
			fprintf(stdout, "PUBLISH %s %lu\n", m.topic.c_str(), m.body.size());

		}
		else if (m.type == "RETRIEVE") {
			fprintf(stdout, "RETRIEVE %s\n", m.sender.c_str());

		}
		else if (m.type == "DISCONNECT") {
			fprintf(stdout, "DISCONNECT %s %ld\n", m.sender.c_str(), m.nonce);
		}
		else if (m.type == "MESSAGE"){
			cout << "TOPIC\t= " << m.topic << endl;
			cout << "USER_ID\t= " << m.sender << endl;
			cout << "LENGTH\t= " << m.body.size() << endl;
			cout << "BODY\t= " << m.body << endl;
		}
	}
};

class App_callback : public Callback {
	void run(Message &m) {
		cout << m.sender << " says: " << m.body << endl;
	}
};
