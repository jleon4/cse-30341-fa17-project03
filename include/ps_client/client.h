// client.h: PS Client Library -------------------------------------------------
#pragma once
#include <string>
#include <queue>
#include "ps_client/thread.h"
#include "ps_client/callback.h"
#include "ps_client/queue.h"
#include <map>

using namespace std;

#define info_log(M) \
  cout << "[" << time(NULL) << "] INFO " << M << endl;

#define error_log(M) \
  cout << "[" << time(NULL) << "] ERROR " << M << endl;

//Thread fucntions
void *publish_func(void *arg); //thread will publish to server
void *retrieve_func(void *arg); //thread will recieve from server

class Client {
  public:
    //This constructs a Client object with the specified server host, server port, and client identify cid.
    Client(const char *host, const char *port, const char *cid);
    //destructor
    ~Client();
    //This schedules a message with the specified topic, message, and length to be published to the server.
    void publish(const char *topic, const char *message, size_t length);
    //This schedules a subscription to the topic be sent to the server, and records a Callback for that particular topic.
    void subscribe(const char *topic, Callback *callback);
    //This schedules an unsubscribe operation from the topic to be sent to the server, and removes all Callbacks for that particular topic.
    void unsubscribe(const char *topic);
    //This schedules a disconnect operation and causes the client to shutdown.
    void disconnect();
    //This starts any required background messaging threads and then processes incoming messages by calling the appropriate Callback on any messages until it is time to shutdown.
    void run();
    //This returns whether or not the client should shutdown.
    bool shutdown();
    //Get Functions
    const char * get_host();
    const char * get_port();
    size_t get_nonce();
    const char * get_client_id();
    Queue<string>* get_send_queue();
    Queue<Message>* get_recieve_queue();
    map<string,Callback*> get_topic_map();
    const char * get_topic();

    //Set Functions
    void set_host(const char* host);
    void set_port(const char* port);
    void set_nonce(size_t nonce);
    void set_client_id(const char *client_id);
    void set_topic(const char * topic);
    //locks
    pthread_mutex_t send_lock;
    pthread_mutex_t callback_lock;



  private:
    size_t nonce;
    const char* host;
    const char* port;
    const char* client_id;
    const char* topic;

    Queue<string>  send_queue; //these will be the concurrent data structure
    Queue<Message>  recv_queue;
    map<string, Callback*> topic_map; //will hold topics and callbacks
    bool finished; //to be used with disconnect and shutdown
};


// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
