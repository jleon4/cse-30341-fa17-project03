#pragma once
#include <string>
using namespace std;
typedef struct {
  string type;        // Message type (MESSAGE, IDENTIFY, SUBSCRIBE, UNSUBSCRIBE, RETRIEVE, DISCONNECT)
  string topic;       // Message topic
  string sender;      // Message sender
  size_t nonce;       // Sender's nonce
  string body;        // Message body
  //string userid;		// User ID
  //int length;			// Length of message
} Message;
