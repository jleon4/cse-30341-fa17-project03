// // queue.h: Queue
//
// #pragma once
//
// #include "ps_client/macros.h"
//
// #include <queue>
//
// #include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
//
// #include <semaphore.h>
// using namespace std;
// const size_t QUEUE_MAX = 64;
//
// template <typename T>
// class Queue {
//
//   public:
//     Queue(){
//     	sem_init(&mutex, 0, 1);
//     	sem_init(&full , 0, 0);
//     	sem_init(&empty, 0, QUEUE_MAX);
//     }
//
//     T pop() {
//     	sem_wait(&full);
//
//     	sem_wait(&mutex);
// 	    T value = data.front();
// 	    if (value != sentinel) {
// 	      data.pop();
// 	    }
//     	sem_post(&mutex);
//
// 	    if (value != sentinel) {
// 	      sem_post(&empty);
// 	    } else {
// 	      sem_post(&full);
// 	    }
//     	return value;
//     }
//
//     void push(const T &value) {
//     	sem_wait(&empty);
//     	sem_wait(&mutex);
//
// 	    data.push(value);
//
//     	sem_post(&mutex);
//     	sem_post(&full);
//     }
//
//     queue<T> get_data(){
//       return data;
//     }
//
//   private:
//     queue<T>   data;
//     T		    sentinel;
//     sem_t	    mutex;
//     sem_t	    full;
//     sem_t	    empty;
//
// };

#pragma once

#include "macros.h"

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

const size_t QUEUE_MAX = 64;

using namespace std;

// static bool operator!=(Message const& m1, Message const& m2) {
//   return m1.nonce != m2.nonce;
// }

template <typename T>
class Queue {
private:
    queue<T>   data;
    pthread_mutex_t lock;
    pthread_cond_t  fill;
    pthread_cond_t  empty;

public:
    Queue(){
    	int rc;
    	Pthread_mutex_init(&lock, NULL);
    	Pthread_cond_init(&fill, NULL);
    	Pthread_cond_init(&empty, NULL);
    }

    void push(const T &value) {
    	int rc;

    	Pthread_mutex_lock(&lock);
    	while (data.size() >= QUEUE_MAX) {
    	    Pthread_cond_wait(&empty, &lock);
	    }

    	data.push(value);
        	Pthread_cond_signal(&fill);
        	Pthread_mutex_unlock(&lock);
        }

    T pop() {
    	int rc;

    	Pthread_mutex_lock(&lock);
    	while (data.empty()) {
    	    Pthread_cond_wait(&fill, &lock);
	    }

    	T value = data.front();

    	    data.pop();
    	   Pthread_cond_signal(&empty);
    	   Pthread_cond_signal(&fill);
        	Pthread_cond_signal(&empty);
        	Pthread_mutex_unlock(&lock);
        	return value;
    }

    queue<T> get_data(){
           return data;
         }

};
