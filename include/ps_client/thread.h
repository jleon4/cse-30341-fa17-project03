//thread.h
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "ps_client/macros.h"

typedef void *(*thread_func)(void *);

class Thread {
	public:
		void start(thread_func func, void *arg);
		void join (void **result);
		void detach();
		pthread_t get_thread();
		void set_thread(pthread_t thread);

	private:
		pthread_t thread;
};
