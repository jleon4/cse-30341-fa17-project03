// app.cpp ---------------------------------------------------------------

// SYSTEM INCLUDES
#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <poll.h>

///afs/nd.edu/user15/pbui/pub/bin/ps_server -p 9412
//./app_test localhost 9412 abe group

// C++ PROJECT INCLUDES
 #include <ps_client/client.h>
 #include "ps_client/callback.h"
 #include "ps_client/macros.h"
 #include "ps_client/message.h"
 #include "ps_client/thread.h"

// Constants -------------------------------------------------------------------
const int   TIMEOUT = 1000;
// Generator -------------------------------------------------------------------

void *messanger_func(void *arg) {
    Client *client = (Client *)arg;
    string input;
    char buffer[BUFSIZ];

    while(!client->shutdown()){

      //Poll stdin
      struct pollfd pfd = {STDIN_FILENO, POLLIN|POLLPRI, 0};
      int    result     = poll(&pfd, 1, TIMEOUT);

      /* Process event */
      if (result < 0) {           /* Error */
        fprintf(stderr, "Unable to poll: %s\n", strerror(errno));
      }
      else {                    /* Handle command */
        if (fgets(buffer, BUFSIZ, stdin) == NULL) {
          break;
        }
        client->publish(client->get_topic(), buffer, strlen(buffer));
      }
    }


    client->disconnect();
    return NULL;
}

// Main execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage %s: host port client_id topic\n", argv[0]);
    	return EXIT_FAILURE;
    }

    const char *host      = argv[1];
    const char *port      = argv[2];
    const char *client_id = argv[3];
    const char *topic = argv[4];

    Client       client(host, port, client_id);
    client.set_topic(topic);
    Thread       messanger;
    App_callback e;

    cout << "Welcome to the Messaging App, you have subscribed to the group: " << topic << endl;

    messanger.start(messanger_func, (void *)&client);


    client.subscribe(topic, &e);
    client.run();
    messanger.join(NULL);
    return EXIT_SUCCESS;
}



// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
