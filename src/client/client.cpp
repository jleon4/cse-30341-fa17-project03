//implementation of client class - client.cpp
#include <iostream>
#include <cmath>
#include <map>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "ps_client/client.h"
#include "ps_client/message.h"
#include "ps_client/queue.h"

#include <pthread.h>
using namespace std;

//This constructs a Client object with the specified server host, server port, and client identify cid.
Client::Client(const char *host, const char *port, const char *cid){
  //set variables
  set_host(host);
  set_port(port);
  set_client_id(cid);
  this->finished = false; //to be used with the shutdown and disconnect
  set_nonce(get_nonce()); //sets nonce to random number
  //initialize locks
  int rc;
  Pthread_mutex_init(&(this->send_lock), NULL);
  Pthread_mutex_init(&(this->callback_lock), NULL);
}


//destructor
Client::~Client() {}

//This schedules a message with the specified topic, message, and length to be published to the server.
void Client::publish(const char *topic, const char *message, size_t length){
  string msg = "PUBLISH ";
	msg.append(topic);
	msg = msg + " ";
	msg.append(to_string(length));
	msg = msg + "\n";
	msg.append(message);
	send_queue.push(msg);
}
//This schedules a subscription to the topic be sent to the server, and records a Callback for that particular topic.
void Client::subscribe(const char *topic, Callback *callback){
  string msg = "SUBSCRIBE ";
  msg.append(topic);
  msg = msg + "\n";
  send_queue.push(msg);
  //add to topic map
  int rc;
	string topic_string = string(topic);
  //Pthread_mutex_lock(&(this->callback_lock));
  topic_map[topic_string] = callback;
  //Pthread_mutex_unlock(&(this->callback_lock));


}
//This schedules an unsubscribe operation from the topic to be sent to the server, and removes all Callbacks for that particular topic.
void Client::unsubscribe(const char *topic){
	string msg = "UNSUBSCRIBE ";
	msg.append(topic);
	msg = msg + "\n";
  send_queue.push(msg);
  //delete from topic map
  auto it=topic_map.find(topic);
  topic_map.erase(it);

}
//This schedules a disconnect operation and causes the client to shutdown.
void Client::disconnect(){
  string msg = "DISCONNECT ";
  msg.append(client_id);
  msg = msg + " ";
  msg = msg + to_string(nonce);
  msg = msg + "\n";
  send_queue.push(msg);

  //now set disconnect flag to true
  int rc;
  Pthread_mutex_lock(&(this->send_lock)); //to prevent racecond or deadlock
  finished = true; //should signal shutdown
  Pthread_mutex_unlock(&(this->send_lock));

  Message mesg;
  mesg.type = "DISCONNECT";
  recv_queue.push(mesg);


}
//This starts any required background messaging threads and then processes incoming messages by calling the appropriate Callback on any messages until it is time to shutdown.
void Client::run(){
  //We only need two more threads as we are already running on a thread now
  Thread send_thread;
  send_thread.start(publish_func, this); //this in this case is the client
  Thread retrieve_thread;
  retrieve_thread.start(retrieve_func, this);


  //Process Message

  Message msg;
  while (!this->shutdown()){
    //pop meessge from recieve queue
    msg = this->get_recieve_queue()->pop();

    //If type is disconnect, shut down
    if (msg.type.compare("DISCONNECT") == 0) {
      info_log("PROCESSING THREAD SHUTTING DOWN");
      break;
    }

    //use map to run callback that corresponds to that meessge
    map<string, Callback*> topic_map = this->get_topic_map();
    Callback *callback = topic_map[msg.topic];
    // use run function from callback on the mesage that was popped.
    callback->run(msg);
  }

  //join is the same as waiting for threads.
  send_thread.join(NULL);
  retrieve_thread.join(NULL);

}


//This returns whether or not the client should shutdown.
bool Client::shutdown(){
  int rc;
  Pthread_mutex_lock(&(this->send_lock)); //lock(&slock)
	bool result = finished;
	Pthread_mutex_unlock(&(this->send_lock));  // unlock(&slock);
	return result;
}

//Get Functions
const char * Client::get_host() {
  return host;
}
const char * Client::get_port() {
  return port;
}
size_t Client::get_nonce() {
  srand (time(NULL));
  int nonceint = rand() % 1000;
  size_t nonce = (size_t)nonceint;

  return nonce;
}
const char * Client::get_client_id() {
  return client_id;
}

const char * Client::get_topic(){
  return topic;
}
Queue<string>* Client::get_send_queue(){
  return &send_queue;
}

Queue<Message>* Client::get_recieve_queue(){
  return &recv_queue;
}

map<string,Callback*> Client::get_topic_map(){
  return topic_map;
}






//Set Functions
void Client::set_host(const char* host) {
  this->host = host;

}
void Client::set_port(const char* port) {
  this->port = port;

}
void Client::set_nonce(size_t nonce) {
  this->nonce = nonce;

}
void Client::set_client_id(const char* client_id){
  this->client_id = client_id;
}

void Client::set_topic(const char * topic){
  this->topic = topic;
}
