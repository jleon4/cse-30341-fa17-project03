// implementation of Socket
#include <iostream>
#include <fstream>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "ps_client/client.h"
#include "ps_client/message.h"
#include <sstream>
#include <vector>


using namespace std;

FILE * Connect(const char *host, const char *port){
  /* Lookup server address information */
    struct addrinfo *results;
    struct addrinfo  hints = {};
    hints.ai_family = AF_UNSPEC;  /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* Use TCP */
    int status;
    if ((status = getaddrinfo(host, port, &hints, &results)) != 0) {
    	fprintf(stderr, "getaddrinfo failed: %s\n", gai_strerror(status));
	return NULL;
    }

    /* For each server entry, allocate socket and try to connect */
    int client_fd = -1;
    for (struct addrinfo *p = results; p != NULL && client_fd < 0; p = p->ai_next) {
	/* Allocate socket */
	if ((client_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
	    continue;
	}

	/* Connect to host */
	if (::connect(client_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
	    close(client_fd);
	    client_fd = -1;
	    continue;
	}
    }

    /* Release allocate address information */
    freeaddrinfo(results);

    if (client_fd < 0) {
    	return NULL;
    }

    /* Open file stream from socket file descriptor */
    FILE *client_file = fdopen(client_fd, "w+");
    if (client_file == NULL) {
        fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
        close(client_fd);
        return NULL;
    }

    return client_file;
}

//This function will return Message struct with the msg
Message to_message(string msg, Client * client, size_t &length) {
  Message result;

  stringstream ss(msg);
  vector<string> avector;
  string object;

  while(ss >> object) {
    avector.push_back(object);
  }


  for (int i = 0; i < avector.size(); i++){
    if (i == 0){
      result.type = avector[i];
    }
    if (i == 1){
      result.topic = avector[i];
    }
    if (i == 3){
      result.sender = avector[i];
    }
    if (i == 5){
      length = stoi(avector[i]);
    }
	}

  result.nonce = client->get_nonce();
  return result;
}


void* publish_func(void *arg) {
  Client *client = (Client *) arg;

  //need to get identify message
  string identify = "IDENTIFY ";
  identify.append(client->get_client_id());
  identify = identify + " ";
  identify.append(to_string(client->get_nonce()));
  identify = identify + "\n";


  //first we need to connect to server using sockets
  FILE * client_file = Connect(client->get_host(), client->get_port()); //should connect to server and return client file
  if (client_file == NULL){
    fprintf(stderr, "Unable to connect to socket: %s\n", strerror(errno));
  }
  info_log("Publisher: Connected to " << client->get_host() << ":" <<client->get_port())
  //cout << "Connected to port: " << client->get_port() << endl;

  //write to server and get response

  char buf[BUFSIZ];
  while(1){
    //we need to keep writing to the server as long as there is something in the send queue
      fwrite(identify.c_str(), 1, identify.size(), client_file); //write to server
      //fgets from client file
      if (fgets(buf, BUFSIZ, client_file) != NULL) {
        info_log("Publisher: " << buf);
      }
      else {
        printf("Failed to receive response from server");
        fclose(client_file);
        exit(EXIT_FAILURE);
      }
      //close stream if shutdown
      if (client->shutdown()) {
        info_log("PUBLISH THREAD SHUTTING DOWN");
        fclose(client_file);
        break;
      }

      //we need to pop last element.
      identify = client->get_send_queue()->pop();
  }
  return EXIT_SUCCESS;


}

void* retrieve_func(void *arg) {
  Client *client = (Client *) arg;
  //identify
  string identify = "IDENTIFY ";
  identify.append(client->get_client_id());
  identify = identify + " ";
  identify.append(to_string(client->get_nonce()));
  identify = identify + "\n";

  //connect to server using sockets
  FILE * client_file = Connect(client->get_host(), client->get_port()); //should connect to server and return client file
  if (client_file == NULL){
    fprintf(stderr, "Unable to connect to socket: %s\n", strerror(errno));
  }
  info_log("Subscriber: Connected to " << client->get_host() << ":" <<client->get_port())


  //identify with server
  fwrite(identify.c_str(), 1, identify.size(), client_file);

  //get response from server
  char buf[BUFSIZ];
  string response;
  if (fgets(buf, BUFSIZ, client_file) != NULL) {
    info_log(buf);
  }
  else {
    cout << "Failed to receive a response from server" << endl;
    fclose(client_file);
  }

  //retrieve message
  string message = "RETRIEVE ";
  message.append(client->get_client_id());
  message = message + "\n";

  //now we need to recieve the meesages from server, turn them into Message struct and push to recieve queue
  while(1){

    //some variables
    Message msg;
    size_t length;
    //write to server
    fwrite(message.c_str(), 1, message.size(), client_file);

    //will get back from server and turn message into Message struct
    if (fgets(buf, BUFSIZ, client_file) != NULL) {
      response = string(buf);
      info_log("load_stream header: " << response);

      //cout << "<-----------RESPONSE FROM SERVER AFTER RETRIEVE ----------->" << response << endl;
      msg = to_message(response, client, length);
    }
    else{
      cout << "Failed to receive a response from server" << endl;
      fclose(client_file);
      exit(EXIT_FAILURE);
    }

    //need to read 2nd line of message
    string body = "";
    if (fgets(buf, length + 1, client_file) != NULL) {
      body = string(buf);
      msg.body = body;
      info_log("load_stream body: " << msg.body);
      //cout << "<-----------BODY:  ----------->" << msg.body << endl;

    }
    else {
      cout << "Failed to receive a response from server" << endl;
      fclose(client_file);
      //exit(EXIT_FAILURE);
    }

    //push this message onto recieve queue
    client->get_recieve_queue()->push(msg);

    if (client->shutdown()) {
      info_log("RECIEVE THREAD SHUTTING DOWN");
      //cout << "RECIEVE THREAD SHUTTING DOWN" << endl;
      fclose(client_file);
      break;
    }
  }
  return EXIT_SUCCESS;
}
