#include "ps_client/thread.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "ps_client/message.h"



using namespace std;


void Thread::start(thread_func func, void *arg) {
	int rc;
	PTHREAD_CHECK(pthread_create(&thread, NULL, func, arg));
}

void Thread::join(void **result) {
	int rc;
	PTHREAD_CHECK(pthread_join(thread, result));
}

void Thread::detach() {
	pthread_detach(thread);
}

pthread_t Thread::get_thread(){
	return thread;
}
void Thread::set_thread(pthread_t thread){
	this->thread = thread;
}
