// SYSTEM INCLUDES

#include <gtest/gtest.h>

// C++ PROJECT INCLUDES

// #include "ps_client/client.h"
// #include "ps_client/callback.h"
// #include "ps_client/macros.h"
// #include "ps_client/message.h"
// #include "ps_client/thread.h"

// Thread tests
// TEST(ThreadTest, ThreadStart) {
// 	Thread test_thread;
// 	int counter = 0;
// 	test_thread.start(publish_func, this);
// 	counter++;
//
// 	EXPECT_EQ(1, counter);
// }
//
// // Client tests
// TEST(ClientTest, Constructor) {
// 	Client *client("3211", "1233", "3");
//
// 	EXPECT_EQ("1233", client->get_port());
// 	EXPECT_EQ("3211", client->get_host());
// }
//
// TEST(ClientTest, Publish) {
// 	Client *client("3211", "1233", "3");
// 	int counter = 0;
//
// 	for (int i = 0; i < 5; i++) {
// 		client->publish("echo", "hello", 5);
// 		counter++;
// 	}
//
// 	EXPECT_EQ(5, counter);
// }
//
// TEST(ClientTest, Subscribe) {
// 	Client *client("3211", "1233", "3");
// 	EchoCallback e;
// 	int counter = 0;
//
// 	for (int i = 0; i < 5; i++) {
// 		client->subscribe("echo", e);
// 		counter++;
// 	}
//
// 	EXPECT_EQ(5, counter);
// }
//
// TEST(ClientTest, Unsubscribe) {
// 	Client *client("3211", "1233", "3");
// 	int counter = 0;
//
// 	for (int i = 0; i < 5; i++) {
// 		client->unsubscribe("echo");
// 		counter++;
// 	}
//
// 	EXPECT_EQ(5, counter);
// }
//
// // Callback test
// TEST(CallbackTest, EchoCallback) {
// 	EchoCallback e;
// 	Message m;
// 	m.type = "SUBSCRIBE";
// 	m.topic = "test";
// 	m.sender = "gsirdesh";
// 	m.nonce = 123;
// 	m.body = "This is a test";
//
// 	int counter = 0;
//
// 	for (int i = 0; i < 10; i++) {
// 		e.run(m);
// 		counter++;
// 	}
//
// 	EXPECT_EQ(10, counter);
// }
//
// // Message tests
// TEST(MessageTest, Type) {
// 	Message m;
// 	m.type = "IDENTIFY";
//
// 	EXPECT_EQ("IDENTIFY", m.type);
// }
//
// TEST(MessageTest, Sender) {
// 	Message m;
// 	m.sender = "gsirdesh";
//
// 	EXPECT_EQ("gsirdesh", m.sender);
// }
//
// // Concurrent data structures test
// TEST(DSTest, Concurrency) {
// 	// Create two threads, one producer and one consumer
// 	Queue<int>	q(-1);
// 	Thread		producer;
// 	Thread 		consumer;
//
// 	producer.start(publish_func, &q);
// 	consumer.start(receiver_func, &q);
//
// 	// Join threads
// 	producer.join(NULL);
// 	consumer.join(NULL);
//
// 	// Have producer push, consumer pop
// 	int val = consumer.pop()
// 	producer.push(val);
//
// 	// Check values
// 	EXPECT_EQ(val, producer.pop());
//
// }

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
